import { Injectable } from '@angular/core';
import { Car } from './car';
import { CARS } from './mock-cars';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
public cars: any[] = CARS;
public dataSource = new Subject<any>();
public data = this.dataSource.asObservable();

public dataSource2 = new Subject<any>();
public data2 = this.dataSource2.asObservable();

  constructor() {
    this.getCarS();
   }

  getCarS(): void {
    this.dataSource.next(this.cars);
  }

  getIndex(vin) {
    return this.cars.indexOf(this.cars.find(cr => cr.vin === vin));
  }

  addCarS(car) {
    this.cars.push(car);
    return this.dataSource.next(this.cars);
  }

  updCarS(car) {
    this.cars[this.getIndex(car.vin)] = car;
    this.getCarS();
  }

  delCarS(vin): void {
    const idx = this.getIndex(vin);
    this.cars.splice(idx, 1);
    this.getCarS();
  }

  setData(car) {
    this.dataSource2.next(car);
  }
}
