import { Directive } from '@angular/core';
import { NoPatternValidator } from './input-form.component';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appNoPattern]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NoPatternDirective, multi: true }]
})
export class NoPatternDirective implements Validator {
  constructor() { }
  validate(control: AbstractControl): { [key: string]: any } | null {
    return NoPatternValidator()(control);
}


}
