export interface Car {
    vin?: string;
    year?: number;
    brand?: string;
    color?: string;
    prices?: number;
    type?: string;
}
