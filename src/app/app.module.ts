import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { AppComponent } from './app.component';
import { InputFormComponent } from './input-form/input-form.component';
import { DataTableComponent } from './data-table/data-table.component';
import { NoNullDirective } from './input-form/no-null.directive';
import { NoPatternDirective } from './input-form/no-pattern.directive';

@NgModule({
   declarations: [
      AppComponent,
      InputFormComponent,
      DataTableComponent,
      NoNullDirective,
      NoPatternDirective
   ],
   imports: [
      ReactiveFormsModule,
      BrowserModule,
      BrowserAnimationsModule,
      FormsModule,
      HttpClientModule,
      TableModule,
      ButtonModule,
      DialogModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
